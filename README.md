# Pac-Man Game with Audio Control (Speech Recognition) 

This is a project for Advanced Programming Course at [Amirkabir University of Technology](http://aut.ac.ir/aut/).
It's a Pac-Man Game written in Python and C++. You can control the game by saying (GO, STOP, UP, DOWN, LEFT, RIGHT, OFF).
It's based on [tensorflow](https://www.tensorflow.org/tutorials/sequences/audio_recognition), [this](http://www.kiranjose.in/blogs/getting-tensorflow-1-4-on-raspberrypi-3/), [this](https://github.com/Uberi/speech_recognition) and [this](https://github.com/TomAtterton/Pacman-QT-Game).

### More Details
- A computer game, written in C++ (Qt) with audio control using speech recognition with Python and Tensorflow,
Advanced Programming course project, Dr. Amir Jahanshahi
- In fact, the user can control the Pac-Man game by just pronouncing ”up”, ”down”, ”left”, ”stop”, ”start” and
 ”go”. The user can play the game in both Windows OS and Linux OS. We used TensorFlow and Google Dataset for training our network.
- Also, the game itself was a fork of a repository in GitHub which we had to rewrite most of it and improve the graphical view of the game.

### Prerequisites

* Qt (if you want to compile the game yourself)
* Anaconda
* Tensorflow
* PyAutoGui
* PyAudio

### How to Run The Game

```
cd mainController && python main.py
```


## Built With

* [Qt](https://www.qt.io/)
* [Tensorflow](https://www.tensorflow.org/)

